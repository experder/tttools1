<?php

namespace ttt1\unicode\source;

use tt\features\config\v1\ConfigProject;
use tt\features\config\v1\ConfigServer;
use tt\features\templates\tt_template\ServiceTtTemplate;
use tt\features\thirdparty\AssetAtom;
use tt\features\thirdparty\v1\Thirdpartyasset;
use tt\services\ServiceArchives;
use tt\services\ServiceFiles;

class Noto extends Thirdpartyasset
{

	const FILE_TTF = "NotoColorEmoji-Regular.ttf";
	const FILE_CSS = "unicode.css";

	/*
	 * https://fonts.google.com/noto/specimen/Noto+Color+Emoji
	 */
	const source = "https://fonts.google.com/download?family=Noto%20Color%20Emoji";

	/**
	 * @return AssetAtom[]
	 */
	function newAtoms() {
		return array(
			self::FILE_TTF=>new AssetAtom($this, AssetAtom::DELIVER_TYPE_NONE, self::FILE_TTF, "ed84f46d3d5564a08541cd64bddd495c"),
			self::FILE_CSS=>new AssetAtom($this, AssetAtom::DELIVER_TYPE_CSS, ConfigProject::$SKIN_ID.'/'.self::FILE_CSS, "21fd2fa77890416e60483b151527509a"),
		);
	}

	/**
	 * @return void
	 */
	function doInstall() {
		$atom = $this->getAtomByKey(self::FILE_TTF);
		$targetFile = $atom->getFilenameAbs();
		if(!file_exists($targetFile)){
			$tempZip = new AssetAtom($this, AssetAtom::DELIVER_TYPE_NONE, "temp.zip", "497a6598c3aec3438f77538134418d4c");
			$tempfile = $tempZip->getFilenameAbs();
			$targetDir = dirname($tempfile);
			ServiceFiles::download(self::source, $tempfile);
			$tempZip->checkHash(true);
			ServiceArchives::doUnzip($tempfile, $targetDir);
			ServiceFiles::unlink_file($targetDir."/OFL.txt");
		}
		$atom = $this->getAtomByKey(self::FILE_CSS);
		$targetFile = $atom->getFilenameAbs();
		if(!file_exists($targetFile)){
			ServiceTtTemplate::createFile($targetFile,
				dirname(__DIR__)."/css/".ConfigProject::$SKIN_ID."/unicode.css",
				array(
					"@import url(\"https://fonts.googleapis.com/css?family=Noto+Color+Emoji\");"
					=>"@font-face {\n\tfont-family: \"Noto Color Emoji\";\n\tsrc: url(\"../NotoColorEmoji-Regular.ttf\");\n}"
				)
			);
		}
	}
}