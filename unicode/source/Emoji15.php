<?php

namespace ttt1\unicode\source;

use tt\features\thirdparty\AssetAtom;
use tt\features\thirdparty\v1\Thirdpartyasset;
use tt\services\ServiceFiles;

class Emoji15 extends Thirdpartyasset
{

	const DIR = 'unicode/emoji15';
	protected $dir = self::DIR;

	const FILE_KEY_zwj_sequences = "zwj_sequences";

	const SOURCE = "https://unicode.org/Public/emoji/15.0/";

	/**
	 * @return AssetAtom[]
	 */
	function newAtoms()
	{
		return array(
			new AssetAtom($this, AssetAtom::DELIVER_TYPE_NONE, 'emoji-sequences.txt', 'c17bac2efb115f8d0b1785f0849a34e2'),
			new AssetAtom($this, AssetAtom::DELIVER_TYPE_NONE, 'emoji-test.txt', 'b3c7a84a57aee5730898e34dcaa227fd'),
			self::FILE_KEY_zwj_sequences=>new AssetAtom($this, AssetAtom::DELIVER_TYPE_NONE, 'emoji-zwj-sequences.txt', '9d807cce17e3034b4ebf35ecd97285bd'),
		);
	}

	/**
	 * @return void
	 */
	function doInstall()
	{
		foreach ($this->getAtoms() as $atom){
			ServiceFiles::download(self::SOURCE.$atom->getFilenameRel(), $atom->getFilenameAbs());
		}
	}

}