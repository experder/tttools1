<?php

namespace ttt1\unicode\source;

use tt\features\thirdparty\AssetAtom;
use tt\features\thirdparty\v1\Thirdpartyasset;
use tt\services\ServiceFiles;

class Unicode15 extends Thirdpartyasset
{

	const FILE_BLOCKS = "Blocks.txt";

	const source = "https://www.unicode.org/Public/UCD/latest/ucd";

	/**
	 * @return AssetAtom[]
	 */
	function newAtoms() {
		return array(
			self::FILE_BLOCKS=>new AssetAtom($this,
				AssetAtom::DELIVER_TYPE_NONE, self::FILE_BLOCKS, "daffaeadc560b7ddc278dbbf8879f977"),
		);
	}

	/**
	 * @return void
	 */
	function doInstall() {
		foreach ($this->getAtoms() as $key=>$atom){
			ServiceFiles::download(self::source.'/'.$atom->getFilenameRel(), $atom->getFilenameAbs());
		}
	}
}