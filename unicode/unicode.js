const unicode = {}

unicode.setFlags = function(codepoint, ffPrefEmoji, ffHasEmoji, ffHasChar=true, isDisplayable=true){

    tt.ajaxJson(tt_run + "/unicode/api", "cmd=setCodepointFlags"
            +"&codepoint="+encodeURIComponent(codepoint)
            +"&ffPrefEmoji="+encodeURIComponent(ffPrefEmoji)
            +"&ffHasEmoji="+encodeURIComponent(ffHasEmoji)
            +"&ffHasChar="+encodeURIComponent(ffHasChar)
            +"&isDisplayable="+encodeURIComponent(isDisplayable)
        , function(data){

        if(!data.success){
            console.log(data);
            alert("Das hat nicht geklappt.");
            return;
        }

        document.getElementById('editMore'+codepoint).remove();
        document.getElementById('prefSelectorChar'+codepoint).removeAttribute('onclick');
        document.getElementById('prefSelectorEmoji'+codepoint).removeAttribute('onclick');
        document.getElementById('iconEntry'+codepoint).classList.remove('editClass');

        if(ffPrefEmoji){
            document.getElementById('prefSelectorEmoji'+codepoint).style.display='none';
        }else{
            document.getElementById('prefSelectorChar'+codepoint).style.display='none';
        }

    });

}

unicode.setBlock = function(block_id, relevance=null, displayable=null){

    tt.ajaxJson(tt_run + "/unicode/api", "cmd=setBlockFlags"
        +"&block_id="+encodeURIComponent(block_id)
        +"&relevance="+encodeURIComponent(relevance)
        +"&displayable="+encodeURIComponent(displayable)
        , function(data){

            if(!data.success){
                console.log(data);
                alert("Das hat nicht geklappt.");
                return;
            }

            if(displayable!==null){
                document.getElementById('editBlockDisplayable').remove();
            }
            if(relevance!==null){
                document.getElementById('editBlockDispose').remove();
            }

        });

}

unicode.setFaviconDiffers = function(codepoint){
    tt.ajaxJson(tt_run + "/unicode/api", "cmd=setFaviconDiffers"
        +"&codepoint="+encodeURIComponent(codepoint)
        , function(data){
            document.getElementById('faviconEdit'+codepoint).remove();
        });
}

unicode.setHasSkintones = function(codepoint, state){
    tt.ajaxJson(tt_run + "/unicode/api", "cmd=setHasSkintones"
        +"&codepoint="+encodeURIComponent(codepoint)
        +"&state="+encodeURIComponent(state)
        , function(data){
            if(!data.success){
                console.log(data);
                alert("Das hat nicht geklappt.");
                return;
            }
            document.getElementById('editSkintone'+codepoint).remove();
        });
}

