<?php

namespace ttt1\unicode;

use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\Model;
use tt\features\database\v1\WhereEquals;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\javascripts\AjaxEndpoint;
use ttt1\unicode\model\Block;
use ttt1\unicode\model\Codepoint;

class ApiEndpoint extends AjaxEndpoint
{

	private $input;

	public static function getClassPolyfill()
	{
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\services\polyfill\Php5::get_class();
	}

	/**
	 * @param array $input Key-value array
	 * @return array Key-value array
	 */
	public function process(array $input)
	{
		$this->input = $input;
		$cmd = $input['cmd'];
		if($cmd==='setCodepointFlags'){
			return $this->setCodepointFlags();
		}
		if($cmd==='setBlockFlags'){
			return $this->setBlockFlags();
		}
		if($cmd==='setHasSkintones'){
			return $this->setHasSkintones();
		}
		if($cmd==='setFaviconDiffers'){
			return $this->setFaviconDiffers();
		}
		new Error("Unknown command '$cmd'!");
		return array();
	}

	private function setCodepointFlags(){
		$codepoint = $this->input['codepoint'];
		$ffPrefEmoji = $this->input['ffPrefEmoji'];
		$ffHasEmoji = $this->input['ffHasEmoji'];
		$ffHasChar = $this->input['ffHasChar'];
		$isDisplayable = $this->input['isDisplayable'];

		$db = DatabaseHandler::getDefaultDb();
//$ok=true;if(false)
		$ok = $db->updateByArray(Codepoint::tableName, array(
			Codepoint::COL_ff_pref_emoji=>$ffPrefEmoji==="null"?null:($ffPrefEmoji==="true"),
			Codepoint::COL_ff_has_emoji=>$ffHasEmoji==="null"?null:($ffHasEmoji==="true"),
			Codepoint::COL_ff_has_char=>$ffHasChar==="null"?null:($ffHasChar==="true"),
			Codepoint::COL_displayable=>$isDisplayable==="null"?null:($isDisplayable==="true"),
		), array(
			new WhereEquals(Codepoint::COL_codepoint, $codepoint),
		));

		return array(
			"success"=>$ok!==false,
		);
	}

	private function setBlockFlags(){
		$block_id = $this->input['block_id'];
		$relevance = $this->input['relevance'];
		$displayable = $this->input['displayable'];

		$updates = array();
		if($relevance!=='null'){$updates[Block::COL_relevance] = $relevance==='true';}
		if($displayable!=='null'){$updates[Block::COL_displayable] = $displayable==='true';}

		$db = DatabaseHandler::getDefaultDb();
		$ok = $db->updateByArray(Block::tableName,$updates,array(
				new WhereEquals(Model::FIELD_id, $block_id),
			)
		);

		return array(
			"success"=>$ok!==false,
		);
	}

	private function setFaviconDiffers(){
		$codepoint = $this->input['codepoint'];

		$updates = array(
			Codepoint::COL_favicon_differs=>true,
		);

		$db = DatabaseHandler::getDefaultDb();
		$ok = $db->updateByArray(Codepoint::tableName,$updates,array(
				new WhereEquals(Codepoint::COL_codepoint, $codepoint),
			)
		);

		return array(
			"success"=>$ok!==false,
		);
	}

	private function setHasSkintones(){
		$codepoint = $this->input['codepoint'];
		$state = $this->input['state'];

		$updates = array(
			Codepoint::COL_has_skintones => $state==='true',
		);

		$db = DatabaseHandler::getDefaultDb();
		$ok = $db->updateByArray(Codepoint::tableName,$updates,array(
				new WhereEquals(Codepoint::COL_codepoint, $codepoint),
			)
		);

		return array(
			"success"=>$ok!==false,
		);
	}

}