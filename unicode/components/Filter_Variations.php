<?php

namespace ttt1\unicode\components;

use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\db_mysql\DatabaseMySql;
use tt\features\debug\errorhandler\v1\Error;
use tt\services\UNI;
use tt\services\UnicodeIcons;
use ttt1\unicode\model\GroupType;
use ttt1\unicode\SimgleCharDetails;

class Filter_Variations extends Filter
{

	/**
	 * @return string
	 */
	function getName()
	{
		return "Variations";
	}

	/**
	 * @return string
	 */
	function getPrefix()
	{
		return UNI::joiner(UnicodeIcons::adult,UnicodeIcons::fire_engine);
	}

	/**
	 * @inheritDoc
	 */
	function getCodepoints()
	{

		$db = DatabaseHandler::getDefaultDb();
		if(!($db instanceof DatabaseMySql))new Error("SQL!!");//TODO
		$detailsObj = new SimgleCharDetails(null, $this->unicodeHandler);

		$variation = GroupType::GROUP_TYPE_VARIATION;
		$result = $db->select(/** @lang text */ "SELECT name,group_concat(codepoint order by cg.id) cps
FROM unicode_codepoint_group cg
left join unicode_group g on cg.`group`=g.id
where g.`type` =$variation
group by cg.`group` order by g.name");
		$icons = array();
		foreach ($result as $row){
			$name = $row['name'];
			$icons[] = $detailsObj->moreIconSeq(
				"<span class='emojiVariationTitle' title='$name'>$name</span>",
				explode(",",$row['cps'])
			);
		}

		return implode("\n", $icons);
	}

}