<?php

namespace ttt1\unicode\components;

use ttt1\unicode\UnicodeHandler;

abstract class Filter
{

	/**
	 * @var UnicodeHandler $unicodeHandler
	 */
	protected $unicodeHandler;

	/**
	 * @var Filter[]
	 */
	private static $all = null;

	/**
	 * @param UnicodeHandler $unicodeHandler
	 */
	public function __construct(UnicodeHandler $unicodeHandler)
	{
		$this->unicodeHandler = $unicodeHandler;
	}

	/**
	 * @return Filter[]
	 */
	public static function getAll(UnicodeHandler $unicodeHandler)
	{
		if(self::$all===null){
			self::$all=array();
			/** @noinspection PhpForeachOverSingleElementArrayLiteralInspection */
			foreach (array(

				new Filter_Variations($unicodeHandler),

					 ) as $filter){
				self::$all[$filter->getName()]=$filter;
			}
		}
		return self::$all;
	}

	/**
	 * @return Filter|null
	 */
	public static function getByName($id, UnicodeHandler $unicodeHandler)
	{
		$all = self::getAll($unicodeHandler);
		return isset($all[$id])?$all[$id]:null;
	}

	/**
	 * @return string
	 */
	abstract function getName();

	/**
	 * @return string
	 */
	abstract function getPrefix();

	/**
	 * @return int[]|string
	 */
	abstract function getCodepoints();

}