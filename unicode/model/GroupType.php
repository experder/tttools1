<?php

namespace ttt1\unicode\model;

use tt\features\database\schema\Table;
use tt\features\database\v1\Model;

class GroupType extends Model
{

	const GROUP_TYPE_GROUP = 1;
	const GROUP_TYPE_VARIATION = 2;
	const GROUP_TYPE_SYNONYM = 3;

	const tableName = "unicode_group_type";
	protected $tableName = self::tableName;

	protected $name;
	const FIELD_name = "name";

	function getGuiName()
	{
		// TODO: Implement getGuiName() method.
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::FIELD_name)
			->setNotNullable()
			->addIndexUnique($table)
		;
	}

}