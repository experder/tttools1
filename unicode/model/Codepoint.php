<?php

namespace ttt1\unicode\model;

use tt\features\database\schema\Column;
use tt\features\database\schema\Table;
use tt\features\database\v1\Model;

class Codepoint extends Model
{

	const tableName = "unicode";
	protected $tableName = self::tableName;

	protected $name_en;
	const COL_name_en ='name_en';
	protected $info;
	const COL_info ='info';
	protected $codepoint;
	const COL_codepoint ='codepoint';
	protected $block;
	const COL_block ='block';
	protected $ff_pref_emoji;
	const COL_ff_pref_emoji ='ff_pref_emoji';
	protected $ff_has_emoji;
	const COL_ff_has_emoji ='ff_has_emoji';
	protected $ff_has_char;
	const COL_ff_has_char ='ff_has_char';
	protected $displayable;
	const COL_displayable ='displayable';
	protected $has_skintones;
	const COL_has_skintones ='has_skintones';
	protected $favicon_differs;
	const COL_favicon_differs ='favicon_differs';

	function getGuiName()
	{
		// TODO: Implement getGuiName() method.
	}

	public static function getClass() {
		return \tt\services\polyfill\Php5::get_class();
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::COL_name_en)
			->setNotNullable()
			->setDataTypeString(100)
		;
		$table->getColumn(self::COL_info)
			->setDataTypeString(100)
		;
		$table->getColumn(self::COL_codepoint)
			->setDataTypeInteger()
			->addIndex($table, true, 'unicode_unint')
		;
		$table->getColumn(self::COL_block)
			->setDataTypeInteger()
			->addForeignKey($table, Block::tableName)
		;
		$table->getColumn(self::COL_ff_has_char)
			->setDataType(Column::DATATYPE_BOOLEAN)
		;
		$table->getColumn(self::COL_ff_has_emoji)
			->setDataType(Column::DATATYPE_BOOLEAN)
		;
		$table->getColumn(self::COL_ff_pref_emoji)
			->setDataType(Column::DATATYPE_BOOLEAN)
		;
		$table->getColumn(self::COL_displayable)
			->setDataType(Column::DATATYPE_BOOLEAN)
		;
		$table->getColumn(self::COL_has_skintones)
			->setDataType(Column::DATATYPE_BOOLEAN)
		;
		$table->getColumn(self::COL_favicon_differs)
			->setDataType(Column::DATATYPE_BOOLEAN)
			->setDefault("'0'")
		;
	}

	/**
	 * @return int
	 */
	public function getCodepoint()
	{
		return $this->codepoint;
	}

	/**
	 * @return string
	 */
	public function getNameEn()
	{
		return $this->name_en;
	}

	/**
	 * @return string
	 */
	public function getInfo()
	{
		return $this->info;
	}

	/**
	 * @return bool|null
	 */
	public function getFfPrefEmoji()
	{
		return $this->ff_pref_emoji;
	}

	/**
	 * @return bool|null
	 */
	public function getFfHasEmoji()
	{
		return $this->ff_has_emoji;
	}

	/**
	 * @return bool|null
	 */
	public function getFfHasChar()
	{
		return $this->ff_has_char;
	}

	/**
	 * @return bool|null
	 */
	public function getDisplayable()
	{
		return $this->displayable;
	}

	/**
	 * @return bool|null
	 */
	public function getHasSkintones()
	{
		return $this->has_skintones;
	}

	/**
	 * @return bool
	 */
	public function getFaviconDiffers()
	{
		return $this->favicon_differs;
	}

}