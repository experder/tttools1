<?php

namespace ttt1\unicode\model;

use tt\features\database\schema\Column;
use tt\features\database\schema\Table;
use tt\features\database\v1\Model;

class Block extends Model
{

	const tableName = "unicode_block";
	protected $tableName = self::tableName;

	protected $description;
	const COL_description='description';
	protected $desc_prefix;
	const COL_desc_prefix='desc_prefix';
	protected $range_info;
	const COL_range_info='range_info';
	protected $relevance;
	const COL_relevance='relevance';
	protected $displayable;
	const COL_displayable='displayable';

	public static function getClass() {
		return \tt\services\polyfill\Php5::get_class();
	}

	function getGuiName()
	{
		// TODO: Implement getGuiName() method.
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::COL_description)
			->addIndexUnique($table)
		;
		$table->getColumn(self::COL_relevance)
			->setDataType(Column::DATATYPE_BOOLEAN)
			->setNotNullable()
			->setDefault("'1'")
		;
		$table->getColumn(self::COL_displayable)
			->setDataType(Column::DATATYPE_BOOLEAN)
		;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @return string
	 */
	public function getRangeInfo()
	{
		return $this->range_info;
	}

	/**
	 * @return bool
	 */
	public function getDisplayable()
	{
		return $this->displayable;
	}

	/**
	 * @return bool
	 */
	public function getRelevance()
	{
		return $this->relevance;
	}

}