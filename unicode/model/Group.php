<?php

namespace ttt1\unicode\model;

use tt\features\database\schema\Table;
use tt\features\database\v1\Model;

class Group extends Model
{

	const tableName = "unicode_group";
	protected $tableName = self::tableName;

	protected $name;
	const FIELD_name = "name";
	protected $name_prefx;
	/**
	 * @deprecated TODO TYPO
	 */
	const FIELD_name_prefx = "name_prefx";
	protected $type;
	const FIELD_type = "type";
	protected $order_key;
	const FIELD_order_key = "order_key";

	function getGuiName()
	{
		// TODO: Implement getGuiName() method.
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::FIELD_name)
			->setNotNullable()
			->addIndexUnique($table)
		;
		$table->getColumn(self::FIELD_type)
			->setNotNullable()
			->setDataTypeInteger()
			->addForeignKey($table, GroupType::tableName)
		;
	}

}