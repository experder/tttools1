<?php

namespace ttt1\unicode\model;

use tt\features\database\schema\ForeignKey;
use tt\features\database\schema\Table;
use tt\features\database\v1\Model;

class Codepoint_group extends Model
{

	/**
	 * @deprecated TODO TYPO
	 */
	const tabelName = "unicode_codepoint_group";
	protected $tableName = self::tabelName;

	protected $group;
	const FIELD_group = 'group';
	protected $codepoint;
	const FIELD_codepoint = 'codepoint';

	function getGuiName()
	{
		// TODO: Implement getGuiName() method.
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$group=$table->getColumn(self::FIELD_group)
			->setDataTypeInteger()
			->setNotNullable()
#			->addForeignKey($table, Group::tableName)
		;
		$table->getColumn(self::FIELD_codepoint)
			->setDataTypeInteger()
			->setNotNullable()
			->addForeignKey($table, Codepoint::tableName, array(Codepoint::COL_codepoint))
		;
		$table->addForeignKey($fk=new ForeignKey(self::tabelName, "unicode_codepoint_group_group_fk", array($group), Group::tableName, array(Model::FIELD_id)));
		$fk->setActions(ForeignKey::CONSTRAINTACTION_RESTRICT, ForeignKey::CONSTRAINTACTION_CASCADE);
//TODO:Must seperate this table! Other groups can have multiple entries!
//		$table->addIndexUnique(ServiceSchema::columnsByNames(
//			array(Codepoint_group::FIELD_group,Codepoint_group::FIELD_codepoint)));
	}

}