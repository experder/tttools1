<?php

namespace ttt1\unicode;

use tt\core\view\HtmlPage;
use tt\features\config\v1\CFG_S;
use tt\features\config\v1\ConfigServer;
use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\db_mysql\DatabaseMySql;
use tt\features\database\v1\WhereEquals;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\htmlpage\components\Form;
use tt\features\htmlpage\components\FormInputCheckbox;
use tt\features\htmlpage\components\FormSubset;
use tt\features\htmlpage\components\HtmlComponent;
use tt\features\i18n\Trans;
use tt\features\messages\v2\MsgInfo;
use tt\r\RunFromCli;
use tt\services\polyfill\Php7;
use tt\services\ServiceEnv;
use tt\services\ServiceNumbers;
use tt\services\ServiceStrings;
use tt\services\UNI;
use tt\services\UnicodeIcons;
use ttt1\unicode\components\Filter;
use ttt1\unicode\model\Block;
use ttt1\unicode\model\Codepoint;
use ttt1\unicode\model\Codepoint_group;
use ttt1\unicode\model\Group;
use ttt1\unicode\model\GroupType;
use ttt1\unicode\source\Emoji15;

class UnicodeHandler implements RunFromCli
{

	const GETVAL_codepoints = 'codepoints';
	const CP_filter = 'filter';

	const GETVAL_noto = "noto";
	const GETVAL_skintones = "skintones";
	const GETVAL_integer = "integer";
	const GETVAL_emoji = "emoji";
	const GETVAL_wide = "wide";
	const GETVAL_fonts = "fonts";

	const GETVAL_verbose = "verbose";
	const GETVAL_edit = "edit";

	public $isVerbose;
	private $isHex;
	public $isEdit;
	private $onlyEmojis;

	private $codepoints_list_count;
	public $displayOnlyOneBlock=false;
	/**
	 * @var Codepoint|false $displayOnlyOneCodepoint
	 */
	public $displayOnlyOneCodepoint=false;

	public $high_id=false;

	const MAXLENGTH=1072;//1072=Egyptian Hieroglyphs

	/**
	 * @var ViewIcons $view
	 */
	public $view=null;

	public function __construct()
	{
		$this->isHex = !isset($_GET[self::GETVAL_integer]);
		$this->onlyEmojis = isset($_GET[self::GETVAL_emoji]);

		$this->isVerbose = CFG_S::$DEVMODE && isset($_GET[self::GETVAL_verbose]);
		$this->isEdit = CFG_S::$DEVMODE && isset($_GET[self::GETVAL_edit]);
	}

	/**
	 * @param ViewIcons $view
	 */
	public function setView($view)
	{
		$this->view = $view;
	}

	public function getHtml(){
		$codepoints = $this->codepointsFromGet();
		if(is_string($codepoints))return $this->getHtmlList($codepoints);

		$icons=array();
		$skipped = array();
		foreach ($codepoints as $codepoint){
			$span = $this->iconSpanCp($codepoint);
			if($span===false){
				$skipped[] = "0x".dechex($codepoint->getCodepoint());
				continue;
			}
			$icons[] = $span;
		}

		if($skipped && $this->isVerbose){
			new Warning("Skipped: ".implode(", ",$skipped));
		}

		$moreIcons = "";
		$details = "";
		if($this->displayOnlyOneCodepoint){
			$scd = new SimgleCharDetails($this->displayOnlyOneCodepoint, $this);
			$moreIcons = "\n".implode("\n",$scd->getMoreIcons());
			$details = $scd->toHtml();
		}

		return $this->getHtmlList(implode("\n",$icons).$moreIcons, $details);
	}
	private function getHtmlList($iconList, $details=""){
		return $this->header()
			.$this->menue()
			.$this->options()
			."<div class='iconsList'>\n".$iconList."</div>$details";
	}

	private function header_block(){
		$block_id = $this->displayOnlyOneBlock;
		$block = new Block();
		$block->fromDbWhereEqualsId($block_id);
		$desc = $block->getDescription();
		$fromTo = explode("..", $block->getRangeInfo());
		$n=hexdec($fromTo[1])-hexdec($fromTo[0])+1;

		$edit = "";
		if($this->isEdit){
			$editDisplayable = $block->getDisplayable()===null?" <span id='editBlockDisplayable'>"
				."<a class='button' onclick='unicode.setBlock($block_id, null, true)'>A</a>"
				." <a class='button' onclick='unicode.setBlock($block_id, null, false)'>".Php7::mb_chr(0xfab1) ."</a>"
				."</span>":"";
			if($block->getRelevance()){
				$editRelevance = " <a id='editBlockDispose' class='button' onclick='unicode.setBlock($block_id, false)'>"
					.UnicodeIcons::asEmoji(UnicodeIcons::Put_Litter_In_Its_Place_Symbol)."</a>";
			}else{
				$editRelevance = " <a id='editBlockDispose' class='button' onclick='unicode.setBlock($block_id, true)'>"
					.UnicodeIcons::asEmoji(UnicodeIcons::Black_Universal_Recycling_Symbol)."</a>";
			}
			$edit=$editRelevance.$editDisplayable;
		}

		$blockProps = "";
		if(!$block->getRelevance()) $blockProps=UnicodeIcons::asText(UnicodeIcons::Wastebasket)." ";
		$blockProps.=Php7::mb_chr(UnicodeIcons::regional_indicator_symbol_letter_b);

		return "<h1 class='blockHeader'>$blockProps $desc <span class='sub'>"
			.$this->codepointNotation(hexdec($fromTo[0]))
			."..".$this->codepointNotation(hexdec($fromTo[1]))
			.", $n ".Trans::late("symbols")."</span>$edit</h1>";
	}

	private function header(){
		if($this->displayOnlyOneBlock!==false){
			return $this->header_block();
		}
		if($this->displayOnlyOneCodepoint){
			$cp = $this->displayOnlyOneCodepoint;
			return "<h1>".$this->codepointNotation($cp->getCodepoint())." - ".$cp->getNameEn()."</h1>";
		}
		return "";
	}

	private function options(){
		$form = new Form(false, false);
		$form->setMethodGet();
		$form->addHiddenFieldFromGet(self::GETVAL_codepoints);
		$form->add($checkbox=new FormInputCheckbox(self::GETVAL_noto));
		$checkbox->addKeyVal(HtmlComponent::KEY_ONCHANGE,"this.form.submit();");
		$form->add($checkbox=new FormInputCheckbox(self::GETVAL_skintones));
		$checkbox->addKeyVal(HtmlComponent::KEY_ONCHANGE,"this.form.submit();");
		$form->add($checkbox=new FormInputCheckbox(self::GETVAL_integer));
		$checkbox->addKeyVal(HtmlComponent::KEY_ONCHANGE,"this.form.submit();");
		$form->add($checkbox=new FormInputCheckbox(self::GETVAL_emoji));
		$checkbox->addKeyVal(HtmlComponent::KEY_ONCHANGE,"this.form.submit();");
		$form->add($checkbox=new FormInputCheckbox(self::GETVAL_wide));
		$checkbox->addKeyVal(HtmlComponent::KEY_ONCHANGE,"this.form.submit();");
		$form->add($checkbox=new FormInputCheckbox(self::GETVAL_fonts));
		$checkbox->addKeyVal(HtmlComponent::KEY_ONCHANGE,"this.form.submit();");

		if (CFG_S::$DEVMODE) {
			$form->add($devpanel = new FormSubset());
			$devpanel->add($checkbox=new FormInputCheckbox(self::GETVAL_verbose));
			$checkbox->addKeyVal(HtmlComponent::KEY_ONCHANGE,"this.form.submit();");
			$devpanel->add($checkbox=new FormInputCheckbox(self::GETVAL_edit));
			$checkbox->addKeyVal(HtmlComponent::KEY_ONCHANGE,"this.form.submit();");
		}

		return $form->toHtml();
	}

	private function menue(){
		$db = DatabaseHandler::getDefaultDb();
		$blocks = $db->generalQuery(array(
			Block::COL_range_info,
			Block::COL_description,
			Block::COL_displayable,
			Block::COL_desc_prefix,
		),Block::tableName,array(
			new WhereEquals(Block::COL_relevance,1),
		));
		$blocks_li = array();
		foreach ($blocks as $block){
			$desc = $block[Block::COL_description];
			$range = explode("..",$block[Block::COL_range_info]);
			$from = hexdec($range[0]);
			$target = 'block'.$from;
			$highClass="";
			$id = HtmlPage::getNextGlobalId();
			if(ServiceEnv::valueFromGet(self::GETVAL_codepoints)===$target){
				$highClass = " high";
				$this->high_id = $id;
			}
			$blocks_li[$desc] = "<li class='block$highClass' title='".$block[Block::COL_range_info]."' id='$id'>"
				."<span class='menuDescPrefix'>".$block[Block::COL_desc_prefix]."</span> "
				.$this->linkToUnicodeBlock($desc, $target)."</li>";
		}

		$type_groups = GroupType::GROUP_TYPE_GROUP;
$groups="";if($db instanceof DatabaseMySql)//TODO!!!
		$groups = $db->select("SELECT g.name ,g.name_prefx, g.id
FROM unicode_group g
where g.`type` = $type_groups");
		foreach ($groups as $group){
			$name = $group['name'];
			$blocks_li[$name] = "<li class='group'><span class='menuDescPrefix'>".$group['name_prefx']."</span> <a href='"
				.ServiceEnv::updateUrlParam(self::GETVAL_codepoints,"group".$group['id'])
				."'>".$name."</a></li>";
		}

		$type_groups = GroupType::GROUP_TYPE_SYNONYM;
$groups="";if($db instanceof DatabaseMySql)//TODO!!!
			$groups = $db->select("SELECT g.name ,g.name_prefx, g.id
FROM unicode_group g
where g.`type` = $type_groups");
		foreach ($groups as $group){
			$name = $group['name'];
			$blocks_li[$name] = "<li class='synonyms'><span class='menuDescPrefix'>".$group['name_prefx']."</span> <a href='"
				.ServiceEnv::updateUrlParam(self::GETVAL_codepoints,"group".$group['id'])
				."'>".strtolower($name)."</a></li>";
		}

		$blocks_li = array_merge($blocks_li, $this->menue_filter());

		ksort($blocks_li);
		return "<div class='iconMenu hideSpecial' id='unicodeIconMenu'>"
			."<ul>"
			.implode("\n",$blocks_li)
			."</ul>"
			."</div>";
	}

	private function menue_filter(){
		$blocks_li=array();
		foreach (Filter::getAll($this) as $filter){

			$prefix = $filter->getPrefix();
			$name = $filter->getName();

			$blocks_li[$name] = "<li class='filter'><span class='menuDescPrefix'>".$prefix."</span> <a href='"
				.ServiceEnv::updateUrlParam(self::GETVAL_codepoints,self::CP_filter.$name)
				."'>".$name."</a></li>";

		}

		return $blocks_li;
	}

	private function linkToUnicodeBlock($blockname, $codepoints){
		return "<a href='".ServiceEnv::updateUrlParams(array(self::GETVAL_codepoints=>$codepoints))."'>$blockname</a>";

	}

	private function skintone($codepoint, $skintone){
		$char = Php7::mb_chr($codepoint).Php7::mb_chr($skintone);
		return "<span class='skintone'>$char</span>";
	}

	public function iconSpanCp(Codepoint $codepointObj){
		if($this->onlyEmojis&&$codepointObj->getFfHasEmoji()===0&&($this->displayOnlyOneCodepoint!==$codepointObj))return "";
		if($codepointObj->getFfHasChar()===0&&$codepointObj->getFfHasEmoji()===0&&!$this->isVerbose&&!$this->displayOnlyOneCodepoint)return "";

		$codepointDisplay = $this->iconSpanCodepointDisplay($codepointObj);
		if($codepointDisplay===false){return false;}

		$codepoint = $codepointObj->getCodepoint();
		$skintones = isset($_GET[self::GETVAL_skintones])?

			"<br>"
			.$this->skintone($codepoint, 0x1F3FB)//light skin tone
			.$this->skintone($codepoint, 0x1F3FC)//medium-light skin tone
			.$this->skintone($codepoint, 0x1F3FD)//medium skin tone
			.$this->skintone($codepoint, 0x1F3FE)//medium-dark skin tone
			.$this->skintone($codepoint, 0x1F3FF)//dark skin tone


			:"";
		$noto = isset($_GET[self::GETVAL_noto])
			#?" <img class='noto' src='https://emojiapi.dev/api/v1/$code_hex/32.webp' alt='NO'>"
			?" <span class='noto'>".Php7::mb_chr($codepoint)."</span>"
			:"";

		$editMore=$this->isEditCodepoint($codepointObj)?"<div class='editMore' id='editMore$codepoint'>"
			."<a class='button' onclick='unicode.setFlags($codepoint, false, false, true, false)'>Ø</a>"
			." <a class='button' onclick='unicode.setFlags($codepoint, false, false, false, false)'>".Php7::mb_chr(0xfab1)."</a>"
			."<br><a class='button' onclick='unicode.setFlags($codepoint, false, false, true, true)'>T T</a>"
			." <a class='button' onclick='unicode.setFlags($codepoint, true, true, false, true)'>".UnicodeIcons::asEmoji(UnicodeIcons::t_rex)." ".UnicodeIcons::asEmoji(UnicodeIcons::t_rex)."</a>"
			."</div>":"";
		$editMore.=($this->isEditSkintone($codepointObj))
			?"<div id='editSkintone$codepoint'>".Php7::mb_chr($codepoint).Php7::mb_chr(UnicodeIcons::AS_SKINTONE_MEDIUM)
			." <a class='button editMore' onclick='unicode.setHasSkintones($codepoint, true)'>"
			.Php7::mb_chr(UnicodeIcons::thumbs_up_sign).Php7::mb_chr(UnicodeIcons::AS_SKINTONE_MEDIUM)."</a>"
			." <a class='button editMore' onclick='unicode.setHasSkintones($codepoint, false)'>"
			.Php7::mb_chr(UnicodeIcons::thumbs_down_sign)."</a></div>"
			:"";

		$title_info = $codepointObj->getNameEn().($codepointObj->getInfo()?"\n".$codepointObj->getInfo():"");
		$title = htmlentities(strtolower($codepointObj->getInfo()?:$codepointObj->getNameEn()));
		$editClass = $this->iconSpanEditClass($codepointObj);
		$detail = "<div class='name_en' title='".$title_info."'>".$title."</div>"
			."<div class='name_code'>".$this->codepointNotation($codepoint)."</div>"
			.$this->iconSpanAltVariation($codepointObj)
			.$this->iconSpanAltSkintones($codepointObj)
			.$noto
			.$skintones;
		return $this->iconEntry($codepointDisplay, $detail, $editMore, $editClass, "iconEntry$codepoint");
	}

	public function iconEntry($display, $details, $more="", $additionalClasses="", $id=false){
		$id = $id?" id='$id'":"";
		return "<div class='iconEntry$additionalClasses'$id>"
				.$display ."<div class='iconDetails'>$details</div>".$more
			."</div>";
	}

	private function isEditCodepoint($codepointObj){
		return $this->isEdit&&($codepointObj->getDisplayable()===null
				||$codepointObj->getFfPrefEmoji()===null
				||$codepointObj->getFfHasChar()===null
				||$codepointObj->getFfHasEmoji()===null);
	}
	private function isEditSkintone($codepointObj){
		return $this->isEdit&&$codepointObj->getHasSkintones()===null&&$codepointObj->getFfHasEmoji()!==0;
	}

	private function iconSpanAltVariation(Codepoint $codepointObj){
		if($codepointObj->getDisplayable()===0 && !($this->isEditCodepoint($codepointObj)))return "";
		$codepoint = $codepointObj->getCodepoint();

		if($codepointObj->getFfPrefEmoji()===null || $this->isEditCodepoint($codepointObj)){
			return $this->beideVarianten($codepoint);
		}

		if($codepointObj->getFfPrefEmoji()){
			if(!$codepointObj->getFfHasChar())return "";
			return UNI::asText($codepoint);
		}

		if(!$codepointObj->getFfHasEmoji())return "";

		if($this->onlyEmojis){
			return Php7::mb_chr($codepoint);
		}

		return UNI::asEmoji($codepoint);
	}

	private function iconSpanAltSkintones(Codepoint $codepointObj){
		if(!$codepointObj->getHasSkintones())return "";
		$cp = $codepointObj->getCodepoint();
		return " "
			.Php7::mb_chr($cp).Php7::mb_chr(UnicodeIcons::AS_SKINTONE_MEDIUM_LIGHT)
			.Php7::mb_chr($cp).Php7::mb_chr(UnicodeIcons::AS_SKINTONE_MEDIUM_DARK)
			;
	}

	public function codepointNotation($codepoint_dec){
		if($this->isHex)return "0x".strtoupper(dechex($codepoint_dec));
		return $codepoint_dec;
	}

	private function iconSpanCodepointDisplay($codepointObj){
		$ffPrefEmoji = $codepointObj->getFfPrefEmoji();
		if($this->onlyEmojis && $ffPrefEmoji===null){return false;}
		$codepoint = $codepointObj->getCodepoint();

		$details = ServiceEnv::updateUrlParam(UnicodeHandler::GETVAL_codepoints, $codepoint);
		if($this->displayOnlyOneCodepoint&&$this->displayOnlyOneCodepoint->getCodepoint()==$codepoint)$details=false;

		if($this->onlyEmojis && !$ffPrefEmoji){
			$icon = UNI::asEmoji($codepoint);
		}else{
			$icon = Php7::mb_chr($codepoint);
		}

		$class = "";

		if($this->onlyEmojis && CFG_S::$DEVMODE){
			$class=" onlyEmojisDevHide";
		}

		if($details)$icon = "<a href='$details'>".$icon."</a>";
		return "<span class='codepoint_display$class'>$icon</span>";
	}

	private function iconSpanEditClass(Codepoint $codepointObj){
		$editClass = "";
		if($this->isEditCodepoint($codepointObj) || $this->isEditSkintone($codepointObj)){
			$editClass=" editClass";
		}
		if($this->onlyEmojis && !$codepointObj->getFfPrefEmoji()){
			$editClass.=" modifierApplied";
		}
		if($codepointObj->getFfHasChar()===0&&$codepointObj->getFfHasEmoji()===0){
			$editClass.=" noDisplay";
		}
		return $editClass;
	}

	private function beideVarianten($codepoint){
		$text = UNI::asText($codepoint);
		$emoji = UNI::asEmoji($codepoint);
		if($this->isEdit){
			$text = /** @lang text */
				"<a onclick='unicode.setFlags($codepoint, false, true);' class='button editMore'"
				." id='prefSelectorChar$codepoint'>$text</a>";
			$emoji = /** @lang text */
				"<a onclick='unicode.setFlags($codepoint, true, true);' class='button editMore'"
				." id='prefSelectorEmoji$codepoint'>$emoji</a>";
		}
		return $text. " ".$emoji;
	}

	/**
	 * @return Codepoint[]
	 */
	private function codepointsFromGet(){
		$db = DatabaseHandler::getDefaultDb();
		if(!($db instanceof DatabaseMySql))new Error("SQL!!");//TODO

		$codepoints_get = ServiceEnv::valueFromGet(self::GETVAL_codepoints, "");
		if($codepoints_get===''){
			$rand_result = $db->select("SELECT codepoint FROM unicode WHERE ff_has_emoji ORDER BY RAND() LIMIT 1;");//TODO
			$rand = $rand_result[0]['codepoint'];
			$_GET[self::GETVAL_codepoints]="around21_$rand";
			$codepoints_get = ServiceEnv::valueFromGet(self::GETVAL_codepoints);
		}
		$codepoints_list = explode(",", $codepoints_get);
		$this->codepoints_list_count = count($codepoints_list);

		$all_codepoints = array();
		foreach ($codepoints_list as $codepointPair_get){
			$cFP = $this->codepointsFromPair($codepointPair_get);
			if(is_string($cFP))return $cFP;
			$all_codepoints = array_merge($all_codepoints, $cFP);
			if(count($all_codepoints)>self::MAXLENGTH) {
				new Warning("Too many codepoints! Please reduce.");
				return array();
			}
		}
		if(!$all_codepoints)return array();
		ksort($all_codepoints);
		$result = $db->select("SELECT * FROM unicode WHERE codepoint in (".implode(",",$all_codepoints).")");
		$return = array();
		foreach ($result as $row){
			$cp = new Codepoint();
			$codepoint_int = $row[Codepoint::COL_codepoint];
			unset($all_codepoints[ServiceNumbers::leadingZeros($codepoint_int,8)]);
			$cp->setDataArray(array(
				Codepoint::COL_block=>$row[Codepoint::COL_block],
				Codepoint::COL_codepoint=>$codepoint_int,
				Codepoint::COL_info=>$row[Codepoint::COL_info],
				Codepoint::COL_name_en=>$row[Codepoint::COL_name_en],
				Codepoint::COL_ff_pref_emoji=>$row[Codepoint::COL_ff_pref_emoji],
				Codepoint::COL_ff_has_emoji=>$row[Codepoint::COL_ff_has_emoji],
				Codepoint::COL_ff_has_char=>$row[Codepoint::COL_ff_has_char],
				Codepoint::COL_displayable=>$row[Codepoint::COL_displayable],
				Codepoint::COL_has_skintones=>$row[Codepoint::COL_has_skintones],
				Codepoint::COL_favicon_differs=>$row[Codepoint::COL_favicon_differs],
			));
			$return[] = $cp;
		}
		if($this->isVerbose && $all_codepoints){
			$out = array();
			foreach ($all_codepoints as $cp){
				$out[] = "0x".dechex($cp);
			}
			new Warning("Unknown Codepoints: ".implode(", ",$out));
		}
		if(count($return)===1){
			$this->view->prefixWithTitle=false;
			$this->displayOnlyOneCodepoint=$return[0];
		}

		return $return;
	}
	private function codepointsFromPair($codepointPair_get){
		$from=$to=false;
		$db = DatabaseHandler::getDefaultDb();
		if(ServiceStrings::startsWith('block',$codepointPair_get,false)){
			$ref = $this->singleCodepointsParser(substr($codepointPair_get,5));
			if($ref!==false){
				if($db instanceof DatabaseMySql){//TODO
					$group = $db->select("SELECT block FROM unicode WHERE codepoint = $ref;");
					if(!$group){
						new Warning("Codepoint not found: 0x".dechex($ref)." ($ref) ('$codepointPair_get')");
						return array();
					}else{
						$block_id = $group[0]['block'];
						if(!$block_id){
							new Warning("No block found for 0x".dechex($ref)." ($ref)!");
							return array();
						}else{
							$all = $db->select("SELECT codepoint FROM unicode where block= $block_id;");
							$codepoints = array();
							foreach ($all as $cp){
								//TODO:Tiefe!!!!!
								$i = $cp['codepoint'];
								$codepoints[ServiceNumbers::leadingZeros($i,8)]=$i;
							}
							if($this->codepoints_list_count===1){
								$this->view->prefixWithTitle=false;
								$this->displayOnlyOneBlock=$block_id;
							}
							return $codepoints;
						}
					}
				}
			}
		}else if(preg_match("/^group(\\d+)\$/i", $codepointPair_get, $matches)){

			$id = $matches[1];
			$all = $db->select("SELECT codepoint FROM unicode_codepoint_group where `group` = :ID;",array(":ID"=>$id));
			$codepoints = array();
			foreach ($all as $row){
				$cp = $row['codepoint'];
				$codepoints[ServiceNumbers::leadingZeros($cp,8)]=$cp;
			}
//			if($this->codepoints_list_count===1){
//				$this->view->prefixWithTitle=false;
//				$this->displayOnlyOneBlock=$block_id;
//			}
			return $codepoints;

		}else if(preg_match("/^".self::CP_filter."(.+)\$/i", $codepointPair_get, $matches)){

			$id = $matches[1];
			$all = Filter::getByName($id, $this)->getCodepoints();
			if(is_string($all))return $all;
			$codepoints = array();
			foreach ($all as $cp){
				$codepoints[ServiceNumbers::leadingZeros($cp,8)]=$cp;
			}
//			if($this->codepoints_list_count===1){
//				$this->view->prefixWithTitle=false;
//				$this->displayOnlyOneBlock=$block_id;
//			}
			return $codepoints;

		}else if(preg_match("/^around(\\d+)_(.*)/i", $codepointPair_get, $matches)){
			$center = $this->singleCodepointsParser($matches[2]);
			$span = $matches[1]-1;
			if($center!==false && $span>=0){
				$from = $center-floor($span/2);
				$to = $from+$span;
			}
		}else if(preg_match("/^([^-]*)-([^-]*)\$/i", $codepointPair_get, $matches)){
			$from = $this->singleCodepointsParser($matches[1]);
			$to = $this->singleCodepointsParser($matches[2]);
		}else{
			$from = $to = $this->singleCodepointsParser($codepointPair_get);
		}
		if($from===false||($to===false&&$from!==null)){
			new Warning("Invalid Syntax: $codepointPair_get");
			return array();
		}
		if($from===null){
			return array();
		}
		if($from>$to){
			$mem = $to;
			$to = $from;
			$from = $mem;
		}
		$codepoints = array();
		for ($i = $from; $i<=$to; $i++){
			$codepoints[ServiceNumbers::leadingZeros($i,8)]=$i;
		}
		return $codepoints;
	}
	private function singleCodepointsParser($codepoint_get){
		$codepoint_get = trim($codepoint_get);
		if($codepoint_get==='')return false;
		if(ServiceStrings::startsWith("0x",$codepoint_get,false)
			||ServiceStrings::startsWith("U ",$codepoint_get,false)
			||ServiceStrings::startsWith("U+",$codepoint_get,false)
		){
			$codepoint_get = strtolower(trim(substr($codepoint_get,2)));
			$value = @hexdec($codepoint_get);
			if(!$value&&$codepoint_get!=='0')return false;
			if(dechex($value)!==$codepoint_get)return false;
			return $value;
		}
		if(preg_match("/\D/",$codepoint_get)){
			return $this->singleCodepointUnicode($codepoint_get);
		}
		return $codepoint_get;
	}

	private function singleCodepointUnicode($codepoint_get){
		$parts = Php7::mb_str_split($codepoint_get);
		$parts_len = count($parts);
		$cp = Php7::mb_ord($parts[0]);
		if($parts_len===1)return $cp;
		$info=array();
		$ok=true;
		$wildcard=false;
		for ($i = 1; $i < $parts_len; $i++){
			$codepoint = Php7::mb_ord($parts[$i]);
			if($wildcard){
				$wildcard=false;
			}else{
				if(!isset(UnicodeIcons::$all_modifiers[$codepoint])){$ok=false;}
			}
			$info[]="0x".strtoupper(dechex($codepoint));
			if($codepoint===UnicodeIcons::MODIFIER_AND)$wildcard=true;
		}
		if(!$ok){
			new Warning("Invalid Syntax or unknown modifiers: $codepoint_get");
			return null;
		}
		$info_with_links = array();
		foreach ($info as $i){
			$dec = hexdec($i);
			#$i = "<a href='".ServiceEnv::updateUrlParam(self::GETVAL_codepoints,$i)."'>$i</a>";
			$glyph = isset(UnicodeIcons::$all_modifiers[$dec])?UnicodeIcons::$all_modifiers[$dec]:Php7::mb_chr($dec);
			$info_with_links[]="$i ($glyph)";
		}
		$code = $this->codepointNotation($cp);
		$chr = Php7::mb_chr($cp);
		new MsgInfo("<pre class='msgOnlyPre'>Codepoint has modifiers:"
			." $codepoint_get = $code ($chr) + ".implode(" + ",$info_with_links)."</pre>");
		return $cp;
	}

	function runFromCli($args)
	{
		$cmd = count($args)>0?$args[0]:null;
		if($cmd==="1")$this->cli_import_blocks();
		elseif($cmd==="2")$this->cli_import_emoji_variations();
		else echo implode("\n",array(
			"1 = import_blocks",
			"2 = import_emoji_variations",
		));
	}

	private function cli_import_emoji_variations(){
echo "EXIT661";exit;/** @noinspection PhpUnreachableStatementInspection */
		echo ConfigServer::$DB_SCHEMA."\n";
		$db = DatabaseHandler::getDefaultDb();
		if(!($db instanceof DatabaseMySql))new Error("TODO663");//TODO


		$emojiSourceAsset = new Emoji15();
		$emojiSourceAsset->checkInstallDoublecheck();
		$fileAtom = $emojiSourceAsset->getAtomByKey(Emoji15::FILE_KEY_zwj_sequences);
		$list = $fileAtom->getFilenameAbs();

$pagesize = 21;
$page = $_SERVER['argv'][3]?:1;
if($page==="r")$page=rand(1,66);
echo "\nPage $page\n";
		$from = 0;
$from = ($page-1)*$pagesize+/*offset:*/29+1;
		$to = 99999;
$to = $from+($pagesize-1);
		$counter = 0;
$from=0;$to=99999;
		$db->update("DELETE FROM unicode_group WHERE type=".GroupType::GROUP_TYPE_VARIATION);
		foreach (file($list) as $line){
			if(++$counter<$from||$counter>$to)continue;
			$line=trim($line);
			if(!$line||substr($line,0,1)=="#")continue;
#if($counter==52)echo $line;
			if(!preg_match("/^(.*?) *;.*?; (.*?) +#.*\$/", $line, $matches))new Error("Syntax error!678");
			$sequence = $matches[1];
			$title = $matches[2];
			echo "$counter;$sequence;$title;\n";
			$id = $db->insertByArray(Group::tableName, array(
				Group::FIELD_name => $title,
				Group::FIELD_type => GroupType::GROUP_TYPE_VARIATION,
			));
			foreach (explode(" ",$sequence) as $cpHex){
				$db->insertByArray(Codepoint_group::tabelName,array(
					Codepoint_group::FIELD_group => $id,
					Codepoint_group::FIELD_codepoint => hexdec($cpHex),
				));
			}
		}
	}

	private function cli_import_blocks(){
echo "EXIT582";exit;/** @noinspection PhpUnreachableStatementInspection */
		if(!CFG_S::$DEVMODE)new Error("DEV ONLY!");
		$blocks = array();
		#foreach (file(__DIR__."/blocks.txt") as $line){
		foreach (file("https://www.unicode.org/Public/UCD/latest/ucd/Blocks.txt") as $line){
			$line=trim($line);
			if(!$line || substr($line,0,1)==='#')continue;
			if(!preg_match("/^(?<from>[\\dA-F]{4,6})\\.\\.(?<to>[\\dA-F]{4,6}); (?<desc>.*?)\$/", $line, $matches)){
				new Error("Blocks file corrupt.");
			}
			$blocks[] = array(
				'from'=>$matches['from'],
				'to'=>$matches['to'],
				'desc'=>$matches['desc'],
			);
			echo ".";
		}
		echo "\n---\n";
		if(!$blocks)new Error("No blocks.");
		$db = DatabaseHandler::getDefaultDb();
		if(!($db instanceof DatabaseMySql))new Error("TODO415");//TODO

		/** @noinspection SqlWithoutWhere */
		$db->update("UPDATE unicode SET block=NULL;");
		/** @noinspection SqlWithoutWhere */
		$db->update("DELETE FROM unicode_block;");

		foreach ($blocks as $block){
			$from = hexdec($block['from']);
			$to = hexdec($block['to']);
			$desc = $block['desc'];
			$id = $db->insertByArray(Block::tableName,array(
				Block::COL_description=>$desc,
				Block::COL_range_info=>"0x".dechex($from)."..0x".dechex($to),
			));
			$db->update("UPDATE unicode SET block=$id WHERE codepoint BETWEEN $from and $to;");
			echo ".";
		}

		$check = $db->select("SELECT count(*) c FROM unicode WHERE block IS NULL;");
		$ok = $check&&$check[0]['c']===0;

		echo "\n";
		echo $ok?"Success!":"Failed!";

		exit;
	}

}