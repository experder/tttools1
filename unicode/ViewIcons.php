<?php

namespace ttt1\unicode;

use tt\features\config\v1\ConfigProject;
use tt\features\config\v1\ConfigServer;
use tt\features\css\Stylesheet;
use tt\features\htmlpage\view\v1\ViewHtmlNew;
use tt\features\javascripts\ServiceJs;
use ttt1\ModuleTools1;
use ttt1\unicode\model\Block;
use ttt1\unicode\source\Noto;

class ViewIcons extends ViewHtmlNew
{

	const title = "Unicode Icons";

	private $unicodeHandler;
	private $html;

	public function __construct()
	{
		if(isset($_GET[UnicodeHandler::GETVAL_wide]))$this->wideContent=true;
		$this->unicodeHandler = new UnicodeHandler();
		$this->unicodeHandler->setView($this);
		$this->html = $this->unicodeHandler->getHtml();
	}

	/**
	 * @return string
	 */
	public static function getClassPolyfill()
	{
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\services\polyfill\Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getHtml()
	{
		return $this->html;
	}

	/**
	 * @return string
	 */
	function getTitle()
	{
		if($this->unicodeHandler->displayOnlyOneBlock!==false){
			/** @noinspection PhpUnusedLocalVariableInspection */
			return ($x = new Block())->fromDbWhereEqualsId($this->unicodeHandler->displayOnlyOneBlock)->getDescription();
		}
		if($this->unicodeHandler->displayOnlyOneCodepoint){
			return //Php7::mb_chr($this->unicodeHandler->displayOnlyOneCodepoint->getCodepoint()) ." ".
				$this->unicodeHandler->displayOnlyOneCodepoint->getNameEn()
				;
		}
		return self::title;
	}

	public function getCss()
	{
		if(ModuleTools1::$CFG_NOTO_LOCAL){
			$emojiSourceAsset = new Noto();
			$emojiSourceAsset->checkInstallDoublecheck();
		}

		$stylesheets = array(//ModuleTools1::getCss(),
			new Stylesheet(
				ModuleTools1::$CFG_NOTO_LOCAL
					?ConfigServer::$HTTP_THIRDPARTY_DIR.'/Noto/'.ConfigProject::$SKIN_ID . '/unicode.css'
					:ConfigServer::$HTTP_MODULE_ROOT[ModuleTools1::module_id]
						. '/unicode/css/' . ConfigProject::$SKIN_ID . '/unicode.css',
				ConfigServer::$HTTP_MODULE_ROOT[ModuleTools1::module_id]
				. '/unicode/css/' . ConfigProject::$SKIN_ID . '/nightmode_unicode.css'
			),
		);

		if(isset($_GET[UnicodeHandler::GETVAL_fonts])){
			$stylesheets[] = new Stylesheet(
				ConfigServer::$HTTP_MODULE_ROOT[ModuleTools1::module_id]
				. '/unicode/css/' . ConfigProject::$SKIN_ID . '/unicode_fonts.css'
			);
		}

		return $stylesheets;
	}

	public function getJsUrls(){
		return array(
			ConfigServer::$HTTP_MODULE_ROOT[ModuleTools1::module_id] . "/unicode/unicode.js",
		);
	}

	public function getHeadJs(){
		if($id=$this->unicodeHandler->high_id){
			return ServiceJs::onLoad2("
				document.getElementById('unicodeIconMenu').classList.remove('hideSpecial');
				document.getElementById('$id').scrollIntoView({block:'center'});
				document.getElementById('unicodeIconMenu').classList.add('hideSpecial');
			");
		}
		return "";
	}

	/**
	 * @return string|false
	 */
	public function getCssClass(){
		if($this->unicodeHandler->displayOnlyOneCodepoint && $this->unicodeHandler->isEdit){
			return "isEditCss";
		}
		return false;
	}

	public function getMoreHead()
	{
		if($this->unicodeHandler->displayOnlyOneCodepoint && $this->unicodeHandler->displayOnlyOneCodepoint->getFfHasEmoji()){
			$code_hex = dechex($this->unicodeHandler->displayOnlyOneCodepoint->getCodepoint());
			return "<link rel=\"shortcut icon\" href=\"https://emojiapi.dev/api/v1/$code_hex/32.webp\">";
		}
		return "";
	}

}