<?php

namespace ttt1\unicode;

use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\db_mysql\DatabaseMySql;
use tt\features\database\v1\LeftJoinStatement;
use tt\features\database\v1\Model;
use tt\features\database\v1\WhereEquals;
use tt\features\htmlpage\components\Form;
use tt\features\htmlpage\components\FormInputDropdown;
use tt\features\htmlpage\components\FormInputText;
use tt\features\htmlpage\components\HtmlComponent;
use tt\features\messages\v2\MsgConfirm;
use tt\features\messages\v2\MsgWarning;
use tt\services\polyfill\Php7;
use tt\services\ServiceArrays;
use tt\services\ServiceEnv;
use tt\services\UnicodeIcons;
use ttt1\unicode\model\Codepoint;
use ttt1\unicode\model\Codepoint_group;
use ttt1\unicode\model\Group;
use ttt1\unicode\model\GroupType;

class SimgleCharDetails
{

	const CMD_addGroup = "addGroup";
	const POSTVAL_group = "group";
	const POSTVAL_add_group = "add1";
	const POSTVAL_add_group_name = "name";
	const POSTVAL_add_synonym = "add2";

	/**
	 * @var Codepoint $codepoint
	 */
	private $codepoint;
	/**
	 * @var UnicodeHandler $unicodeHandler
	 */
	private $unicodeHandler;

	/**
	 * @param Codepoint $codepoint
	 * @param UnicodeHandler $unicodeHandler
	 */
	public function __construct($codepoint, UnicodeHandler $unicodeHandler)
	{
		$this->codepoint = $codepoint;
		$this->unicodeHandler = $unicodeHandler;
	}

	public function toHtml()
	{
		$this->processAddGroup();

		$icons = array();

		$icons = array_merge($icons, $this->links());

		$icons = array_merge($icons, $this->groups(GroupType::GROUP_TYPE_SYNONYM));

		$icons = array_merge($icons, $this->groups(GroupType::GROUP_TYPE_GROUP));

		if($this->unicodeHandler->isEdit){
			$form1 = $this->addToGroupForm(GroupType::GROUP_TYPE_GROUP);
			$form2 = $this->addToGroupForm(GroupType::GROUP_TYPE_SYNONYM);
			$icons[]="<hr>".$form1->toHtml().$form2->toHtml();
		}

		return implode("\n",$icons);
	}

	private function processAddGroup()
	{
		if(ServiceEnv::valueFromPost(Form::COMMAND_STRING)!==self::CMD_addGroup)return;
		$id = ServiceEnv::valueFromPost(self::POSTVAL_group);
		if(!$id){new MsgWarning("!79");}

		if($id===self::POSTVAL_add_group){
			$name = ServiceEnv::valueFromPost(self::POSTVAL_add_group_name);
			if(!$name){
				new MsgWarning("Please enter new group's name!");
				return;
			}
			$id = DatabaseHandler::getDefaultDb()->insertByArray(Group::tableName, array(
				Group::FIELD_type => GroupType::GROUP_TYPE_GROUP,
				Group::FIELD_name => $name,
				Group::FIELD_name_prefx => Php7::mb_chr($this->codepoint->getCodepoint()),
			));
		}

		if($id===self::POSTVAL_add_synonym){
			$id = DatabaseHandler::getDefaultDb()->insertByArray(Group::tableName, array(
				Group::FIELD_type => GroupType::GROUP_TYPE_SYNONYM,
				Group::FIELD_name => $this->codepoint->getNameEn(),
				Group::FIELD_name_prefx => Php7::mb_chr($this->codepoint->getCodepoint()),
			));
		}

		DatabaseHandler::getDefaultDb()->insertByArray(Codepoint_group::tabelName, array(
			Codepoint_group::FIELD_group => $id,
			Codepoint_group::FIELD_codepoint => $this->codepoint->getCodepoint(),
		));
		new MsgConfirm("Codepoint added to group $id.");
	}

	private function addToGroupForm($TYPE_GROUP)
	{
		$form = new Form(self::CMD_addGroup, false);

		if($TYPE_GROUP===GroupType::GROUP_TYPE_GROUP){
			$add_key = self::POSTVAL_add_group;
			$group_name = "group";
			$addField = new FormInputText(self::POSTVAL_add_group_name, "New group's name");
		}else{
			$add_key = self::POSTVAL_add_synonym;
			$group_name = "like";
			$addField = null;
		}

		$db=DatabaseHandler::getDefaultDb();
$groups="";if($db instanceof DatabaseMySql)//TODO
		$groups = $db->select("SELECT t0.id,t0.name,t0.name_prefx,t0.order_key
FROM `unicode_group` t0
WHERE `type`=$TYPE_GROUP
ORDER BY if(order_key is null,name,order_key)
-- ORDER BY id desc
");

		$options = ServiceArrays::keyValueArray($groups,
			"{".Group::FIELD_name."} {".Group::FIELD_name_prefx."}"
		);
		$options = array("0"=>"", $add_key=>"CREATE ".strtoupper($group_name),)+$options;

		$form->add($addField);
		$form->add($input=new FormInputDropdown(self::POSTVAL_group, $options, "Add ".$group_name));
		$input->addKeyVal(HtmlComponent::KEY_ONCHANGE, "this.form.submit();");

		return $form;
	}

	private function getMoreIcons_asEmoji($codepointObj)
	{
		$codepoint = $codepointObj->getCodepoint();
		if($codepointObj->getFfHasEmoji() && $codepointObj->getFfPrefEmoji()===0){
			return array($this->moreIconSeq(UnicodeIcons::$all_modifiers[UnicodeIcons::AS_EMOJI],array(
				$codepoint,
				UnicodeIcons::AS_EMOJI
			)));
		}
		if ($this->unicodeHandler->isVerbose){
			return array("<div class='debug_tile'>".$this->moreIconSeq(UnicodeIcons::$all_modifiers[UnicodeIcons::AS_EMOJI],array(
					$codepoint,
					UnicodeIcons::AS_EMOJI
				))."</div>");
		}
		return array();
	}

	private function getMoreIcons_asText($codepointObj)
	{
		$codepoint = $codepointObj->getCodepoint();
		if($codepointObj->getFfHasChar() && $codepointObj->getFfPrefEmoji()===1){
			return array($this->moreIconSeq(UnicodeIcons::$all_modifiers[UnicodeIcons::AS_CHAR],array(
				$codepoint,
				UnicodeIcons::AS_CHAR
			)));
		}
		if ($this->unicodeHandler->isVerbose){
			return array("<div class='debug_tile'>".$this->moreIconSeq(UnicodeIcons::$all_modifiers[UnicodeIcons::AS_CHAR],array(
					$codepoint,
					UnicodeIcons::AS_CHAR
				))."</div>");
		}
		return array();
	}


	public function getMoreIcons()
	{
		$codepointObj = $this->codepoint;
		$codepoint = $codepointObj->getCodepoint();
		$icons = array();
		if($codepointObj->getFfPrefEmoji()){
			$icons = array_merge($icons, $this->getMoreIcons_asEmoji($codepointObj));
			$icons = array_merge($icons, $this->getMoreIcons_asText($codepointObj));
		}else{
			$icons = array_merge($icons, $this->getMoreIcons_asText($codepointObj));
			$icons = array_merge($icons, $this->getMoreIcons_asEmoji($codepointObj));
		}
		if(!$this->codepoint->getHasSkintones() && $this->unicodeHandler->isVerbose) {
			$icons[] = "<div class='debug_tile dskintone'>".$this->skintone($codepoint, UnicodeIcons::AS_SKINTONE_MEDIUM)."</div>";
		}
		$icons = array_merge($icons, $this->variations());
		if($codepointObj->getFfHasEmoji()) {
			$icons = array_merge($icons, $this->noto($codepoint));
		}else if ($this->unicodeHandler->isVerbose){
			$icons = array_merge($icons, $this->noto($codepoint, true));
		}
		$icons[] = $this->noto_icon($codepointObj);
		return array_merge($icons, $this->skintones());
	}

	private function noto($codepoint, $debug=false)
	{
		$fonts=array();
		if(isset($_GET[UnicodeHandler::GETVAL_fonts])){
			$fonts[] = $this->moreIcon("<span class='font_mozilla_old'>".Php7::mb_chr($codepoint)."</span>",
				"EmojiOne Mozilla"
			);
			$fonts[] = $this->moreIcon("<span style='font-family: \"Twemoji Mozilla\", sans-serif;'>".Php7::mb_chr($codepoint)."</span>",
				"Twemoji Mozilla"
			);
			$fonts[] = $this->moreIcon("<span class='systemfont_win10'>".Php7::mb_chr($codepoint)."</span>",
				"Windows 10"
			);
			$fonts[] = $this->moreIcon("<span class='systemfont_win11'>".Php7::mb_chr($codepoint)."</span>",
				"Windows 11"
			);
			$fonts[] = $this->moreIcon("<span class='font_dev1'>".Php7::mb_chr($codepoint)."</span>",
				"Open Sans"
			);
			$fonts[] = $this->moreIcon("<span class='font_dev2'>".Php7::mb_chr($codepoint)."</span>",
				"Symbola"
			);
			$fonts[] = $this->moreIcon("<span class='font_dev3'>".Php7::mb_chr($codepoint)."</span>",
				"Samsung"
			);
			$fonts[] = $this->moreIcon("<span class='font_dev4'>".Php7::mb_chr($codepoint)."</span>",
				"One UI"
			);
		}
		$fonts[] = $this->moreIcon("<span class='noto'>".Php7::mb_chr($codepoint)."</span>",
			"Noto"
		);
		if($debug){
			$fonts_debug = array();
			foreach ($fonts as $font){
				$fonts_debug[] = "<div class='debug_tile'>$font</div>";
			}
			$fonts = $fonts_debug;
		}
		return $fonts;
	}
	private function noto_icon(Codepoint $codepointObj)
	{
		$codepoint = $codepointObj->getCodepoint();

		$edit="";

		$code_hex=dechex($codepoint);
		return $this->moreIcon("<img class='apiIcon60' src='https://emojiapi.dev/api/v1/$code_hex/60.webp' alt=''>",
			"Favicon".$edit
			."<br><a href='https://emojiapi.dev/api/v1/$code_hex.svg'>SVG</a>"
		);
	}

	private function skintones()
	{
		if(!$this->codepoint->getHasSkintones())return array();
		$cp = $this->codepoint->getCodepoint();
		$variations = array();

		$variations[] = "<br>";
		$variations[] = $this->skintone($cp, UnicodeIcons::AS_SKINTONE_LIGHT);
		$variations[] = $this->skintone($cp, UnicodeIcons::AS_SKINTONE_MEDIUM_LIGHT);
		$variations[] = $this->skintone($cp, UnicodeIcons::AS_SKINTONE_MEDIUM);
		$variations[] = $this->skintone($cp, UnicodeIcons::AS_SKINTONE_MEDIUM_DARK);
		$variations[] = $this->skintone($cp, UnicodeIcons::AS_SKINTONE_DARK);

		return $variations;
	}
	private function skintone($cp, $skintone)
	{
		return $this->moreIconSeq(UnicodeIcons::$all_modifiers[$skintone],array($cp,$skintone));
	}

	private function variations()
	{
		$variations = array();

		$variations_index = GroupType::GROUP_TYPE_VARIATION;

		$db = DatabaseHandler::getDefaultDb();
		if(!($db instanceof DatabaseMySql))return array();///TODO!!!!
		$groups = $db->select("SELECT `group`,g.name 
FROM unicode_codepoint_group cg
left join unicode_group g on cg.`group` =g.id 
where codepoint = :codepoint and g.`type` = $variations_index
group by cg.`group`;",
			array(":codepoint"=>$this->codepoint->getCodepoint()));

		foreach ($groups as $group){
			$group_id = $group['group'];

			$codepoints = $db->select("SELECT codepoint  
FROM unicode_codepoint_group cg
where `group` = $group_id
order by id;");

			$codepoint_sequence=ServiceArrays::keyValueArray($codepoints, "{codepoint}", false);

			$group_name = $group['name'];
			$group_name = "<span class='emojiVariationTitle' title='$group_name'>$group_name</span>";

			$variations[] = $this->moreIconSeq($group_name, $codepoint_sequence);
		}

		return $variations;
	}

	private function links()
	{
#		$code_hex = strtoupper(dechex($this->codepoint->getCodepoint()));
		#$details = "https://www.compart.com/en/unicode/U+$code_hex";
		#$details = "https://symbl.cc/en/$code_hex";

		$links = array();

		$links[] = "AA";

		return $links;
	}

	private function groups($groups_index)
	{
		$variations = array();

		$db = DatabaseHandler::getDefaultDb();
		if(!($db instanceof DatabaseMySql))return array();///TODO!!!!
		$groups = $db->select("SELECT `group`,g.name ,g.name_prefx
FROM unicode_codepoint_group cg
left join unicode_group g on cg.`group` =g.id 
where codepoint = :codepoint and g.`type` = $groups_index
group by cg.`group`;",
			array(":codepoint"=>$this->codepoint->getCodepoint()));

		foreach ($groups as $group){
			$group_name = $group['name'];
			if($groups_index===GroupType::GROUP_TYPE_SYNONYM){
				$group_name = "<h2 class='emojiGroup'>Like ".$group['name_prefx']."</h2>";
			}else{
				$group_name = "<h2 class='emojiGroup'>$group_name</h2>";
			}
			$variations[] = $group_name;

			$group_id = $group['group'];
			$codepoint_sequence=Model::fromDatabase(Codepoint::getClass(),array(
				new LeftJoinStatement(Codepoint_group::tabelName, Codepoint::COL_codepoint, Codepoint_group::FIELD_codepoint),
				new WhereEquals(Codepoint_group::tabelName.'.'.Codepoint_group::FIELD_group, $group_id),
			));
			foreach ($codepoint_sequence as $item) {
				if(!($item instanceof Codepoint))continue;
				if($item->getCodepoint()==$this->codepoint->getCodepoint())continue;
				$variations[] = $this->unicodeHandler->iconSpanCp($item);
			}
		}

		return $variations;
	}

	private function moreIcon($icon, $details)
	{
		$icon = "<span class='codepoint_display'>$icon</span>";
		return $this->unicodeHandler->iconEntry($icon, $details);
	}

	public function moreIconSeq($desc, array $codepoint_sequence)
	{
		$icon = "";
		$codepoints = array();
		foreach ($codepoint_sequence as $codepoint){
			$icon .= Php7::mb_chr($codepoint);
			$codepoints[]= $this->subSequenceHtml($codepoint);
		}

		$details = $desc."<div class='codepoint_sequence'>".implode("<br>",$codepoints)."</div>";

		return $this->moreIcon($icon, $details);
	}

	private function subSequenceHtml($codepoint)
	{
		$glyphe = isset(UnicodeIcons::$modifiers_short[$codepoint])
			?UnicodeIcons::$modifiers_short[$codepoint]
			:Php7::mb_chr($codepoint);
		return $glyphe." <a href='".ServiceEnv::updateUrlParam(UnicodeHandler::GETVAL_codepoints,$codepoint)."'>"
			.$this->unicodeHandler->codepointNotation($codepoint)."</a>";
	}

}