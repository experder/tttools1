<?php

namespace ttt1;

use tt\features\autoloader\simple1\RootNamespace;
use tt\features\config\v1\ConfigProject;
use tt\features\database\v1\Model;
use tt\features\frontcontroller\bycode\Route;
use tt\features\i18n\Trans;
use tt\features\modulehandler\v1\RegistrationHandler;
use tt\services\UNI;
use tt\services\UnicodeIcons;
use ttt1\unicode\ApiEndpoint;
use ttt1\unicode\model\Block;
use ttt1\unicode\model\Codepoint;
use ttt1\unicode\model\Codepoint_group;
use ttt1\unicode\model\GroupType;
use ttt1\unicode\model\Group;
use ttt1\unicode\ViewIcons;

class ModuleTools1 extends RegistrationHandler
{

	public static $CFG_NOTO_LOCAL = false;

	const module_id = "tttools1";
	private static $namespace_root = 'ttt1';
	const GUI_NAME = "Tools";

	/**
	 * @return Route[]
	 */
	function getModuleRoutes()
	{
		return array(
			new Route("/ttt1/unicode", ViewIcons::getClassPolyfill(), ViewIcons::title),
			new Route("/unicode/api", ApiEndpoint::getClassPolyfill(), false),
		);
	}

	/**
	 * @return string Maximum 40 chars.
	 */
	function getModuleId()
	{
		return self::module_id;
	}

	/**
	 * @return RootNamespace[]|null
	 */
	function getNamespaceRoots()
	{
		return array(
			new RootNamespace(self::$namespace_root, __DIR__),
		);
	}

	/**
	 * @return Model[]
	 */
	function createModelSchema()
	{
		return array(
			new Block(),
			new Codepoint(),
			new GroupType(),
			new Group(),
			new Codepoint_group(),
		);
	}

	public function getI18namespace()
	{
		return self::$namespace_root . '\i18n';
	}

	public function getGuiName()
	{
		if (ConfigProject::$SKIN_BILDER_SAGEN_MEHR_ALS_WORTE) {
			return UNI::asEmoji(UnicodeIcons::hammer_and_wrench);
		}
		return Trans::late(self::GUI_NAME);
	}

}