<?php /** @noinspection PhpUnused See RegistrationHandler::getI18wordlist() */

namespace ttt1\i18n;

use tt\features\i18n\Language;

/**
 * @see RegistrationHandler::getI18wordlist()
 */
class Lang_german extends Language
{

//	const CONTEXT_xxx = "xxx";

	/**
	 * @return string[]
	 */
	function getWordList()
	{
		return array(
			"symbols"=>"Zeichen",
			"Tools"=>"Tools",
		);
	}

	/**
	 * @return string[][]
	 */
	function getWordListWithKontext()
	{
		return array(
//			Lang_german::CONTEXT_=>array(
//				""=>"",
//			),
		);
	}

}